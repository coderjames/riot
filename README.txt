RIoTBoard Development Sandbox

Directory Layout:
	src/	Inputs to the build go here
	obj/	Intermediate files produced during the build go here (e.g. *.o)
	bin/	Outputs from the build go here (e.g. uImage)
	doc/	Documentation goes here

Builds using the ARM GNU GCC Cortex-A toolchain from:
	https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a
Ninja from:
	https://ninja-build.org/
